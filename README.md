# ETD Cookie Plugin

The **ETD Cooki** Plugin is an extension for [Grav CMS](http://github.com/getgrav/grav). The plugin is based on the popular **[Tarte au citron](https://tarteaucitron.io/fr/) JS-library**. 


## Installation

Installing the **Simple Cookie** plugin can be done in one of three ways: The GPM (Grav Package Manager) installation method lets you quickly install the plugin with a simple terminal command, the manual method lets you do so via a zip file, and the admin method lets you do so via the Admin Plugin.

### GPM Installation (Preferred)

To install the plugin via the [GPM](http://learn.getgrav.org/advanced/grav-gpm), through your system's terminal (also called the command line), navigate to the root of your Grav-installation, and enter:

    bin/gpm install etd-cookie

This will install the Simple Cookie plugin into your `/user/plugins`-directory within Grav. Its files can be found under `/your/site/grav/user/plugins/etd-cookie`.

### Manual Installation

To install the plugin manually, download the zip-version of this repository and unzip it under `/your/site/grav/user/plugins`. Then rename the folder to `simple-cookie`. You can find these files on [GitHub](https://github.com/tomschwarz/grav-plugin-simple-cookie) or via [GetGrav.org](http://getgrav.org/downloads/plugins#extras).

You should now have all the plugin files under

    /your/site/grav/user/plugins/etd-cookie


### Admin Plugin

If you use the Admin Plugin, you can install the plugin directly by browsing the `Plugins`-menu and clicking on the `Add` button.

## Configuration

Before configuring this plugin, you should copy the `user/plugins/etd-cookie/etd-cookie.yaml` to `user/config/plugins/etd-cookie.yaml` and only edit that copy.

Here is the default configuration and an explanation of available options:

```yaml
enabled: true
custom: false
position: bottom-right
compliance: info
palette: gray
```

Note that if you use the Admin Plugin, a file with your configuration named etd-cookie.yaml will be saved in the `user/config/plugins/`-folder once the configuration is saved in the Admin.

## Credits

The plugin is based on the [Tarte au citron](https://tarteaucitron.io/fr/) JS-libary which is developed by [ETD Solutions](https://etd-solutions.com/).

## Contributing

If you want to contribute create an issue or an pull request.  
I appreciate every single help!

## Other

If you got some problems, improvements or changes let me know.  
